TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := mips

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/mips-linux-musl-mips32/bin/mips-linux-musl-
TARGET_UBOOT_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/mips-mti-linux-gnu/bin/mips-mti-linux-gnu-

TARGET_LINUX_IMAGE := uImage

TARGET_GLOBAL_CFLAGS := -march=mips32

HOST_CPP = cc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

TARGET_SKEL_DIRS := $(TOP_DIR)/trail/malta/config/

TARGET_UBI_IMAGE_OPTIONS := \
       --mkubifs="-m 1 -e 65408 -c 272 -x favor_lzo -F" \
       --ubinize="-p 0x10000 -m 1 $(TARGET_CONFIG_DIR)/ubinize.config"
